# translation of ksmserver.po to Turkish
# Copyright (C) 2004, 2008 Free Software Foundation, Inc.
#
# Görkem Çetin <gorkem@kde.org>, 2004.
# Serdar Soytetir <tulliana@gmail.com>, 2008, 2009, 2010, 2012.
# Volkan Gezer <volkangezer@gmail.com>, 2013, 2014, 2017, 2022.
# Kaan Ozdincer <kaanozdincer@gmail.com>, 2014.
# Emir SARI <emir_sari@icloud.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-27 02:13+0000\n"
"PO-Revision-Date: 2023-03-15 11:53+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "Oturumu kapatma, '%1' tarafından iptal edildi"

#: main.cpp:74
#, kde-format
msgid "$HOME not set!"
msgstr "$HOME ayarlanmamış!"

#: main.cpp:78 main.cpp:86
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr "$HOME dizini (%1) yok."

#: main.cpp:81
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""
"$HOME dizinine yazma izni yok (%1). Bu bilginiz dahilindeyse ortamınızda "
"<envar>KDE_HOME_READONLY=1</envar> çevre değişkenini ayarlayın."

#: main.cpp:88
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr "$HOME dizinini (%1) okuma izni yok."

#: main.cpp:92
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr "$HOME dizini (%1) dolu."

#: main.cpp:95
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr "$HOME dizinine (%2) yazarken '%1' hatası oluştu"

#: main.cpp:108 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr "'%1' üzerine yazma izni yok."

#: main.cpp:110 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr "'%1' için okuma izni yok."

#: main.cpp:118 main.cpp:131
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr "Geçici dosya dizini (%1) dolu."

#: main.cpp:121 main.cpp:134
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""
"Geçici dosya dizinine (%2) yazma\n"
"    '%1' hatası ile başarısız oldu"

#: main.cpp:149
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""
"Plasma başlatılırken aşağıdaki kurulum\n"
"sorunu algılandı:"

#: main.cpp:152
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""
"\n"
"\n"
"Plasma başlatılamıyor.\n"

#: main.cpp:159
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr "Plasma Çalışma Alanı kurulum sorunu!"

#: main.cpp:193
#, kde-format
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"Standart X11R6 oturum yönetim protokolünü (XSMP) kullanan \n"
"güvenilir Plasma oturum yöneticisi."

#: main.cpp:197
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Olanaklıysa kaydedilen oturumu geri yükler"

#: main.cpp:200
#, kde-format
msgid "Also allow remote connections"
msgstr "Uzaktan bağlantılara izin ver"

#: main.cpp:203
#, kde-format
msgid "Starts the session in locked mode"
msgstr "Oturumu kilitli kipte başlatır"

#: main.cpp:207
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""
"Kilit ekranı desteği olmadan başlatılır. Yalnızca diğer bileşen kilit "
"ekranını sağlarsa gereklidir."

#: server.cpp:881
#, kde-format
msgid "Session Management"
msgstr "Oturum Yönetimi"

#: server.cpp:886
#, kde-format
msgid "Log Out"
msgstr "Oturumu Kapat"

#: server.cpp:891
#, kde-format
msgid "Shut Down"
msgstr "Kapat"

#: server.cpp:896
#, kde-format
msgid "Reboot"
msgstr "Yeniden Başlat"

#: server.cpp:902
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Onaysız Oturumu Kapat"

#: server.cpp:907
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr "Onaysız Kapat"

#: server.cpp:912
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Onaysız Yeniden Başlat"
