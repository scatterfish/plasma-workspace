# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-02 01:43+0000\n"
"PO-Revision-Date: 2022-12-27 18:49+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.12.3\n"

#: kcm.cpp:73
#, kde-format
msgid "Toggle do not disturb"
msgstr "بدل وضع عدم الإزعاج"

#: sourcesmodel.cpp:391
#, kde-format
msgid "Other Applications"
msgstr "التطبيقات الأخرى"

#: ui/ApplicationConfiguration.qml:91
#, kde-format
msgid "Show popups"
msgstr "أظهر المنبثقات"

#: ui/ApplicationConfiguration.qml:105
#, kde-format
msgid "Show in do not disturb mode"
msgstr "أظهر في وضع عدم الإزعاج"

#: ui/ApplicationConfiguration.qml:118 ui/main.qml:147
#, kde-format
msgid "Show in history"
msgstr "أظهر في التاريخ"

#: ui/ApplicationConfiguration.qml:129
#, kde-format
msgid "Show notification badges"
msgstr "أظهر شارات الإشعار"

#: ui/ApplicationConfiguration.qml:162
#, fuzzy, kde-format
#| msgid "Configure Events…"
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "اضبط الأحداث..."

#: ui/ApplicationConfiguration.qml:170
#, fuzzy, kde-format
#| msgid ""
#| "This application does not support configuring notifications on a per-"
#| "event basis."
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr "لا يدعم هذا التطبيق تكوين الإشعارات على أساس كل حدث."

#: ui/ApplicationConfiguration.qml:257
#, kde-format
msgid "Show a message in a pop-up"
msgstr ""

#: ui/ApplicationConfiguration.qml:266
#, kde-format
msgid "Play a sound"
msgstr ""

#: ui/main.qml:41
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"تعذر العثور على أداة \"الإشعارات\" المطلوبة لعرض الإشعارات. تأكد من تمكينها "
"إما في صينية النظام أو كأداة مستقلة."

#: ui/main.qml:52
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "الإشعارات تزود حاليا بواسطة '%1 %2' عوضا عن بلازما."

#: ui/main.qml:56
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "الإشعارات لا تزود بواسطة بلازما حاليًا."

#: ui/main.qml:63
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "نمط عدم الإزعاج"

#: ui/main.qml:68
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "Enable:"
msgstr "مكّن:"

#: ui/main.qml:69
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "عندما تكون الشاشات معكوسة"

#: ui/main.qml:81
#, kde-format
msgctxt "Enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "أثناء مشاركة الشاشة"

#: ui/main.qml:96
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Keyboard shortcut:"
msgstr "اختصار لوحة المفاتيح:"

#: ui/main.qml:103
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "شروط الظهور"

#: ui/main.qml:108
#, kde-format
msgid "Critical notifications:"
msgstr "إشعارات حرجة:"

#: ui/main.qml:109
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "أظهر في وضع عدم الإزعاج"

#: ui/main.qml:121
#, kde-format
msgid "Normal notifications:"
msgstr "إشعارات عادية:"

#: ui/main.qml:122
#, kde-format
msgid "Show over full screen windows"
msgstr "أظهر فوق نوافذ مالئة الشاشة"

#: ui/main.qml:134
#, kde-format
msgid "Low priority notifications:"
msgstr "إشعارات ذات أولوية منخفضة:"

#: ui/main.qml:135
#, kde-format
msgid "Show popup"
msgstr "أظهر المنبثقات"

#: ui/main.qml:164
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "المنبثقات"

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "المكان:"

#: ui/main.qml:171
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "قرب أيقونة الإشعار"

#: ui/main.qml:208
#, kde-format
msgid "Choose Custom Position…"
msgstr "اختر موضع مخصص..."

#: ui/main.qml:217 ui/main.qml:233
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "0 ثانية"
msgstr[1] "ثانية واحدة"
msgstr[2] "ثانيتين"
msgstr[3] "%1 ثواني"
msgstr[4] "%1 ثانية"
msgstr[5] "%1 ثانية"

#: ui/main.qml:222
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "أخف بعد:"

#: ui/main.qml:245
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "تغذية إضافية"

#: ui/main.qml:250
#, kde-format
msgid "Application progress:"
msgstr "تقدم التطبيق:"

#: ui/main.qml:251 ui/main.qml:291
#, kde-format
msgid "Show in task manager"
msgstr "أظهر في مدير المهام"

#: ui/main.qml:263
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "أظهر في التنبيهات"

#: ui/main.qml:277
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "أبقِ النوافذ المنبثقة مفتوحة أثناء التقدم "

#: ui/main.qml:290
#, kde-format
msgid "Notification badges:"
msgstr "شارات إشعار:"

#: ui/main.qml:302
#, kde-format
msgctxt "@title:group"
msgid "Application-specific settings"
msgstr "إعدادات مخصصة لتطبيقات معينة"

#: ui/main.qml:307
#, kde-format
msgid "Configure…"
msgstr "اضبط…"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "موضع المنبثقات"

#: ui/SourcesPage.qml:19
#, kde-format
msgid "Application Settings"
msgstr "إعدادات التطبيق"

#: ui/SourcesPage.qml:99
#, kde-format
msgid "Applications"
msgstr "التطبيقات"

#: ui/SourcesPage.qml:100
#, kde-format
msgid "System Services"
msgstr "خدمات النظام"

#: ui/SourcesPage.qml:148
#, fuzzy, kde-format
#| msgid "No application or event matches your search term."
msgid "No application or event matches your search term"
msgstr "لا يوجد تطبيق أو حدث يطابق مصطلح البحث الخاص بك."

#: ui/SourcesPage.qml:171
#, fuzzy, kde-format
#| msgid ""
#| "Select an application from the list to configure its notification "
#| "settings and behavior."
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr "حدد تطبيقًا من القائمة لتكوين إعدادات الإشعارات وسلوكه."

#~ msgid "Configure Notifications"
#~ msgstr "اضبط الإشعارات"

#~ msgid "This module lets you manage application and system notifications."
#~ msgstr "تتيح لك هذه الوحدة إدارة إشعارات التطبيق والنظام."

#~ msgctxt "Turn do not disturb mode on/off with keyboard shortcut"
#~ msgid "Toggle with:"
#~ msgstr "بدل بواسطة:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "زايد السعيدي"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zayed.alsaidi@gmail.com"

#~ msgid "Notifications"
#~ msgstr "الإشعارات"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uwe Broulik"
