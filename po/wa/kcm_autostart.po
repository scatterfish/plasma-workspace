# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jean Cayron <jean.cayron@gmail.com>, 2008, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-26 01:47+0000\n"
"PO-Revision-Date: 2010-10-22 18:25+0200\n"
"Last-Translator: Jean Cayron <jean.cayron@gmail.com>\n"
"Language-Team: Walloon <linux@walon.org>\n"
"Language: wa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: autostartmodel.cpp:374
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:377
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:380
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:383
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: ui/entry.qml:52
#, fuzzy, kde-format
#| msgid "Name"
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "No"

#: ui/entry.qml:57
#, fuzzy, kde-format
#| msgid "Status"
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Estat"

#: ui/entry.qml:62
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr ""

#: ui/entry.qml:71
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr ""

#: ui/entry.qml:71
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "A l' enondaedje"

#: ui/entry.qml:105
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr ""

#: ui/entry.qml:109
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr ""

#: ui/main.qml:42
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:56
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:58
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr ""

#: ui/main.qml:80
#, kde-format
msgid "Add Application…"
msgstr ""

#: ui/main.qml:85
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "Radjouter scripe..."

#: ui/main.qml:90
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "Radjouter scripe..."

#: ui/main.qml:130
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""

#: ui/main.qml:154
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr ""

#: ui/main.qml:159 unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr ""

#: ui/main.qml:165
#, fuzzy, kde-format
#| msgid "&Properties"
msgctxt "@action:button"
msgid "See properties"
msgstr "&Prôpietés"

#: ui/main.qml:171
#, fuzzy, kde-format
#| msgid "&Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "&Oister"

#: ui/main.qml:181
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:184
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:187
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "Å pré-enondaedje di KDE"

#: ui/main.qml:190
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: ui/main.qml:198
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:199
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: ui/main.qml:214
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "Radjouter scripe..."

#: ui/main.qml:234
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr ""

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr ""

#: unit.cpp:28
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "A l' enondaedje"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr ""

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr ""

#: unit.cpp:153
#, kde-format
msgid "Failed to open journal"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Djan Cayron"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "jean.cayron@gmail.com"

#, fuzzy
#~| msgid "KDE Autostart Manager Control Panel Module"
#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Module do scriftôr di controle do manaedjeu d' enondaedje tot seu"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Copyright © 2006-2010 Ekipe do manaedjeu d' enondaedje tot seu"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Mintneu"

#, fuzzy
#~| msgid "Advanced"
#~ msgid "Add..."
#~ msgstr "Sipepieus"

#, fuzzy
#~| msgid "Shell script:"
#~ msgid "Shell script path:"
#~ msgstr "Scripe shell:"

#~ msgid "Create as symlink"
#~ msgstr "Ahiver come loyén simbolike"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "N' enonder tot seu ki dins KDE"

#~ msgid "Command"
#~ msgstr "Comande"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Enonder"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "Manaedjeu d' enondaedje tot seu di KDE"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "En alaedje"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Essocté"

#~ msgid "Desktop File"
#~ msgstr "Fitchî d' sicribanne "

#~ msgid "Script File"
#~ msgstr "Fitchî scripe"

#~ msgid "Add Program..."
#~ msgstr "Radjouter programe..."

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "I gn a k' les fitchîs avou des cawetes \".sh\" ki sont permetowes pos "
#~ "apontyî l' evironmint."

#~ msgid "Shutdown"
#~ msgstr "Å distindaedje"

#~ msgid "1"
#~ msgstr "1"
