# Burkhard Lück <lueck@hube-lueck.de>, 2014, 2016, 2018, 2021.
# Frederik Schwarzer <schwarzer@kde.org>, 2016, 2022, 2023.
# Jannick Kuhr <opensource@kuhr.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-31 01:57+0000\n"
"PO-Revision-Date: 2023-08-14 21:37+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.11.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Deutsches KDE-Übersetzerteam"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-i18n-de@kde.org"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Feiertage"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Termine"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Aufgaben"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Weitere"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 Termin"
msgstr[1] "%1 Termine"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Keine Termine"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:113
#, kde-format
msgid "Days"
msgstr "Tage"

#: calendar/qml/MonthViewHeader.qml:119
#, kde-format
msgid "Months"
msgstr "Monate"

#: calendar/qml/MonthViewHeader.qml:125
#, kde-format
msgid "Years"
msgstr "Jahre"

#: calendar/qml/MonthViewHeader.qml:163
#, kde-format
msgid "Previous Month"
msgstr "Vorheriger Monat"

#: calendar/qml/MonthViewHeader.qml:165
#, kde-format
msgid "Previous Year"
msgstr "Vorheriges Jahr"

#: calendar/qml/MonthViewHeader.qml:167
#, kde-format
msgid "Previous Decade"
msgstr "Vorheriges Jahrzehnt"

#: calendar/qml/MonthViewHeader.qml:184
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Heute"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgid "Reset calendar to today"
msgstr "Kalender auf den heutigen Tag zurücksetzen"

#: calendar/qml/MonthViewHeader.qml:196
#, kde-format
msgid "Next Month"
msgstr "Nächster Monat"

#: calendar/qml/MonthViewHeader.qml:198
#, kde-format
msgid "Next Year"
msgstr "Nächstes Jahr"

#: calendar/qml/MonthViewHeader.qml:200
#, kde-format
msgid "Next Decade"
msgstr "Nächstes Jahrzehnt"

#: calendar/qml/MonthViewHeader.qml:256
#, kde-format
msgid "Keep Open"
msgstr "Offen halten"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Einrichten ..."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Bildschirmsperre aktiviert"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr ""
"Legt fest, ob der Bildschirm nach einer angegebenen Zeit gesperrt wird."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Bildschirmschoner-Startzeit"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Legt die Zeit fest, nach der der Bildschirm gesperrt wird."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Neue Sitzung"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filter"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:41
#, kde-format
msgid "Select Plasmoid File"
msgstr "Plasmoid-Datei auswählen"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:65
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Die Installation des Pakets %1 ist fehlgeschlagen."

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:65
#, kde-format
msgid "Installation Failure"
msgstr "Installationsfehler"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Accessibility"
msgstr "Zugangshilfen"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Application Launchers"
msgstr "Anwendungsstarter"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Astronomy"
msgstr "Astronomie"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Date and Time"
msgstr "Datum und Zeit"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Development Tools"
msgstr "Entwicklungswerkzeuge"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Education"
msgstr "Bildung"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Environment and Weather"
msgstr "Umwelt und Wetter"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "Examples"
msgstr "Beispiele"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "File System"
msgstr "Dateisystem"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Spaß und Spiele"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Graphics"
msgstr "Grafik"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Language"
msgstr "Sprache"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Mapping"
msgstr "Kartographie"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Miscellaneous"
msgstr "Verschiedenes"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Multimedia"
msgstr "Multimedia"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Online Services"
msgstr "Online-Dienste"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "Productivity"
msgstr "Produktivität"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "System Information"
msgstr "Systeminformation"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Utilities"
msgstr "Dienstprogramme"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr "Fenster und Prozesse"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Clipboard"
msgstr "Zwischenablage"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:133
msgctxt "applet category"
msgid "Tasks"
msgstr "Prozesse"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:149
#, kde-format
msgid "All Widgets"
msgstr "Alle Miniprogramme"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:153
#, kde-format
msgid "Running"
msgstr "Laufende Miniprogramme"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:159
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Nicht installierbar"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:163
#, kde-format
msgid "Categories:"
msgstr "Kategorien:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:233
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Neue Miniprogramme herunterladen"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:242
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Miniprogramm aus lokaler Datei installieren ..."

#~ msgid "&Execute"
#~ msgstr "&Ausführen"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Vorlagen"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Arbeitsumgebungs-Skriptkonsole"

#~ msgid "Editor"
#~ msgstr "Editor"

#~ msgid "Load"
#~ msgstr "Laden"

#~ msgid "Use"
#~ msgstr "Verwenden"

#~ msgid "Output"
#~ msgstr "Ausgabe"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "Die Skriptdatei <b>%1</b> kann nicht geladen werden"

#~ msgid "Open Script File"
#~ msgstr "Skriptdatei öffnen"

#~ msgid "Save Script File"
#~ msgstr "Skriptdatei speichern"

#~ msgid "Executing script at %1"
#~ msgstr "Skript wird ausgeführt an %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Laufzeit: %1 ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Hintergrundbild-Module herunterladen"

#~ msgid "Containments"
#~ msgstr "Container"

#~ msgctxt ""
#~ "%1 is a type of widgets, as defined by e.g. some plasma-packagestructure-"
#~ "*.desktop files"
#~ msgid "Download New %1"
#~ msgstr "Herunterladen neuer %1"
